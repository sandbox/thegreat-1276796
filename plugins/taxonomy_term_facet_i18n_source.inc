<?php

class TaxonomyTermFacetI18nSource extends AbstractFacetI18nSource {

  public function translate(array $map) {
    $items = entity_load('taxonomy_term', array_keys($map));

    foreach ($items as $key => $item) {
      $map[$key] = i18n_taxonomy_term_name($item, $this->langcode);
    }

    return $map;
  }
}
