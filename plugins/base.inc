<?php

abstract class AbstractFacetI18nSource {
  protected $langcode;

  public function __construct(array $options) {
    if (!isset($options['langcode'])) {
      $language = i18n_language();
      $this->langcode = $language->language;
    }
    else {
      $this->langcode = $options['langcode'];
    }
  }

  abstract public function translate(array $map);
}
